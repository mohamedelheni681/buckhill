## STEPS TO RUN THE PROJECT


```bash
git clone https://gitlab.com/mohamedelheni681/buckhill.git
```
```bash
cd buckhill/buckill_api
```
```bash
cp .env.example .env
```
```bash
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs
```
```bash
./vendor/bin/sail up -d
```
```bash
./vendor/bin/sail artisan migrate --seed
```
```bash
./vendor/bin/sail artisan key:generate
```
```bash
./vendor/bin/sail artisan jwt:secret
```
```bash
./vendor/bin/sail artisan cache:clear
```
```bash
./vendor/bin/sail artisan config:clear
```
```bash
./vendor/bin/sail artisan l5-swagger:generate
```
## API Swagger:
#### http://localhost/api/documentation

#### Admin:
email: admin@buckhill.co.uk

password: admin

## phpMyAdmin:
#### http://localhost:8080/

Username: sail

password: password

## To Run endpoints tests:
```bash
./vendor/bin/sail artisan test
```
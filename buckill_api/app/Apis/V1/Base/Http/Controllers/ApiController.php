<?php
namespace App\Apis\V1\Base\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 *@OA\Info(
 *     title="Your super ApplicationAPI",
 *     version="1.0.0",
 *     )
 */
abstract class ApiController extends Controller
{
    /**
     * @OA\OpenApi(
     *   @OA\Server(
     *      url="/api/v1"
     *   ),
     *   @OA\Info(
     *      title="Swagger-Demo",
     *      version="1.0.0",
     *   ),
     * )
     */

    /**
     *@OA\Tag(name="UnAuthorize", description="No user login required")
     */

    /**
     *@OA\Tag(name="Authorize", description="User login required")
     */

    /**
     * @OA\SecurityScheme(
     *   securityScheme="Bearer",
     *   type="apiKey",
     *   in="header",
     *   name="Authorization"
     * )
     */

    const STATUS_SUCCESS = 200;
    const STATUS_CREATED = 201;
    const STATUS_DELETED = 202;
    const STATUS_INVALID_PARAM = 400;
    const STATUS_INTERNAL_ERROR = 500;
    const STATUS_NOT_AUTHORIZED = 401;


    /**
     * @param $error
     * @return mixed
     */
    public function getErrorMsg($error){
        $errorMsg = [
            'email_password_incorrect' => 'The Email or Password you entered is incorrect.',
            'unauthorized'             => 'Unauthorized'
        ];

        return $errorMsg[$error];
    }

    /**
     * @param $msg
     * @return mixed
     */
    public function getSuccessMsg($msg){
        $successMsg = [
            'authenticated_successfully'   => 'You have been authenticated successfully.',
            'account_created_successfully' => 'Your account has been created successfully.',
            'logged_successfully'          => 'You have been logged out successfully.',
            'connected_user_data'          => 'Connected User data.',
        ];

        return $successMsg[$msg];
    }
}

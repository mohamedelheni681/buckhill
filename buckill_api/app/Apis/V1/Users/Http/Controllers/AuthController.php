<?php

namespace App\Apis\V1\Users\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Apis\V1\Base\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Hash;
use Nette\Utils\DateTime;


class AuthController extends ApiController
{

    /**
     * @OA\Post  (
     *     tags={"Admin"},
     *     path="/api/v1/admin/login",
     *     summary="user login",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *       required={"email", "password"},
     *                 @OA\Property(
     *                     property="email",
     *                     type="string",
     *                     default="test@example.com"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string",
     *                     default="password"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(response="401", description="fail"),
     *     @OA\Response(response="200", description="An example resource", @OA\JsonContent(type="object", @OA\Property(format="string", default="20d338931e8d6bd9466edeba78ea7dce7c7bc01aa5cc5b4735691c50a2fe3228", description="token", property="token"))),
     * )
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->APIResponse(null, null, $validator->errors(), 0, static::STATUS_INVALID_PARAM);
        } else {
            if (! $token = Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return response()->APIResponse(null, null, $this->getErrorMsg('email_password_incorrect'), 0, static::STATUS_INVALID_PARAM);

            } else {
                Auth::user()->last_login_at = new DateTime();
                Auth::user()->save();
                $data = [
                    'token' => $this->createNewToken($token)->original["access_token"]
                ];
                return response()->APIResponse($data, $this->getSuccessMsg('authenticated_successfully'),null, 1, static::STATUS_SUCCESS);
            }
        }
    }


    /**
     * @OA\Post  (
     *     tags={"Admin"},
     *     path="/api/v1/admin/create",
     *     summary="user register",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *       required={"first_name", "last_name", "email", "password", "password_confirmation", "address", "phone_number"},
     *                 @OA\Property(
     *                     property="first_name",
     *                     type="string",
     *                     default="first_name"
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     type="string",
     *                     default="last_name"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string",
     *                     default="test@example.com"
     *                 ),
     *                @OA\Property(
     *                     property="password",
     *                     type="string",
     *                     default="password"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     type="string",
     *                     default="password"
     *                 ),
     *                 @OA\Property(
     *                     property="avatar",
     *                     type="string",
     *                     default="avatar"
     *                 ),
     *                 @OA\Property(
     *                     property="address",
     *                     type="string",
     *                     default="address"
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     type="string",
     *                     default="phone_number"
     *                 ),
     *                @OA\Property(
     *                     property="is_marketing",
     *                     type="boolean",
     *                     default=true
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response="401", description="fail"),
     *     @OA\Response(response="200", description="An example resource", @OA\JsonContent(type="object", @OA\Property(format="string", default="20d338931e8d6bd9466edeba78ea7dce7c7bc01aa5cc5b4735691c50a2fe3228", description="token", property="token"))),
     * )
     */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|string|confirmed|min:5',
            'address'       => 'required',
            'phone_number'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->APIResponse(null, null, $validator->errors(), 0, static::STATUS_INVALID_PARAM);
        } else {
            $userObj = $this->createAdminUser($request);
            if ($userObj->save()) {
                $data = [
                    'user_id'        => $userObj->id,
                    'first_name'     => $request->firstname,
                    'last_name'      => $request->lastname,
                    'avatar'         => $request->avatar,
                    'email'          => $request->email,
                    'address'        => $request->address,
                    'phone_number'   => $request->phone_number,
                    'is_marketing'   => $request->is_marketing,
                ];

                return response()->APIResponse($data, $this->getSuccessMsg('account_created_successfully'), null, 1, static::STATUS_CREATED);
            }
        }
    }

    /**
     * @OA\Get(
     *     tags={"Admin"},
     *     path="/api/v1/admin/logout",
     *     summary="Logs out current logged in user session",
     *     security={{ "bearer":{} }},
     *     @OA\Response(
     *         response=200,
     *         description="Success"
     *     ),
     * )
     *
     * Logs out current logged in user session.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $user = auth()->user();
        if($user){
            $user->tokens()->delete();
            return response()->APIResponse(null, $this->getSuccessMsg('logged_successfully'), null, 1, static::STATUS_SUCCESS);
        }else{
            return response()->APIResponse(null, null, null, 1, static::STATUS_SUCCESS);
        }
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => 'Bearer ' . $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    /**
     * @return mixed
     */
    public function unauthenticated(){
        return response()->APIResponse(null, null, $this->getErrorMsg('unauthorized'), 0, static::STATUS_NOT_AUTHORIZED);;
    }

    /**
     * @param Request $request
     * @return User
     */
    public function createAdminUser(Request $request)
    {
        return new User([
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'avatar' => $request->avatar,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'phone_number' => $request->phone_number,
            'is_marketing' => $request->is_marketing ? User::IS_MARKETING : User::IS_NOT_MARKETING,
            'is_admin' => User::IS_ADMIN,
        ]);
    }

}
<?php
namespace App\Apis\V1\Users\Http\Controllers;

use App\Apis\V1\Base\Http\Controllers\ApiController;
use App\Apis\V1\Exceptions\ApiNotFoundException;
use App\Apis\V1\Users\Http\Requests\UsersRequest;
use App\Apis\V1\Users\Http\Resources\UserResource;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UsersController extends ApiController
{
    /**
     * @OA\Get(
     *     tags={"User"},
     *     path="/api/v1/user",
     *     summary="get user list",
     *     security={{ "bearer":{} }},
     *     @OA\Response(response="401", description="fail"),
     *     @OA\Response(response="200", description="success"))
     * )
     */

    public function getUser(Request $request)
    {
        $data = Auth::user();
        return response()->APIResponse($data, $this->getSuccessMsg('connected_user_data'), null, 1, static::STATUS_SUCCESS);

    }

}

<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        response()->macro('APIResponse', function ($data = null, $messages = null, $errors = null, $status = 1, $code = 200) {
            return $this->json([
                'status'  => $status,
                'message' => $messages,
                'errors'  => $errors,
                'data'    => $data
            ], $code);
        });
    }
}
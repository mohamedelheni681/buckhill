<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    public function testUserIsLoggedOutProperly()
    {
        $faker = Faker::create();

        $email = $faker->email;
        $password = $faker->password(6);

        $user = User::create([
            'firstname' => $faker->firstName,
            'lastname'  => $faker->lastName,
            'avatar'    => null,
            'email'     => $email,
            'password'  => Hash::make($password),
            'address'   => $faker->address,
            'phone_number' => $faker->phoneNumber,
            'is_marketing' => false,
        ]);

        $payload = ['email' => $email, 'password' => $password];

        $response = $this->json('POST', 'api/v1/admin/login', $payload);

        $headers = ['Authorization' => $response['data']['token']];

        $this->json('get', '/api/v1/user', [], $headers)->assertStatus(200);
        $this->json('get', '/api/v1/admin/logout', [], $headers)->assertStatus(200);

        $user = User::find($user->id);

        $this->assertEquals(null, $user->api_token);
    }

    public function testUserWithNullToken()
    {
        $this->json('get', '/api/v1/user', [], [])->assertStatus(401);
    }
}

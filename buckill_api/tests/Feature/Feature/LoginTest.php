<?php

namespace Tests\Feature\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function testRequiresEmailAndLogin()
    {
        $this->json('POST', 'api/v1/admin/login')
            ->assertStatus(400)
            ->assertJson([
                'status'  => 0,
                'message' => null,
                'errors'  => [
                    'email' => [
                        'The email field is required.'
                    ],
                    'password' => [
                        'The password field is required.'
                    ]
                ],
                'data' => null
            ]);
    }

    public function testDefaultAdminLoginSuccessfully()
    {
        $payload = ['email' => 'admin@buckhill.co.uk', 'password' => 'admin'];

        $this->json('POST', 'api/v1/admin/login', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'message',
                'errors',
                'data' => ['token'],
            ]);

    }

    public function testCreatedAdminLoginSuccessfully()
    {
        $faker = Faker::create();

        $email = $faker->email;
        $password = $faker->password(6);

        User::create([
            'firstname' => $faker->firstName,
            'lastname'  => $faker->lastName,
            'avatar'    => null,
            'email'     => $email,
            'password'  => Hash::make($password),
            'address'   => $faker->address,
            'phone_number' => $faker->phoneNumber,
            'is_marketing' => false,
        ]);

        $payload = ['email' => $email, 'password' => $password];

        $this->json('POST', 'api/v1/admin/login', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'message',
                'errors',
                'data' => ['token'],
            ]);

    }
}

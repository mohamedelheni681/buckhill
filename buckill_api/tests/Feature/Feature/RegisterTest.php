<?php

namespace Tests\Feature;

use Tests\TestCase;
use Faker\Factory as Faker;

class RegisterTest extends TestCase
{
    public function testsRegistersSuccessfully()
    {

        $faker = Faker::create();

        $password = $faker->password(6);

        $payload = [
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'avatar'       => null,
            'email'        => $faker->email,
            'password'     => $password,
            'password_confirmation' => $password,
            'address'      => $faker->address,
            'phone_number' => $faker->phoneNumber,
            'is_marketing' => false,
        ];

        $this->json('post', '/api/v1/admin/create', $payload)
            ->assertJsonStructure([
                'status',
                'message',
                'errors',
                'data',
            ])->assertStatus(201);
    }

    public function testsRequiresFields()
    {
        $this->json('post', '/api/v1/admin/create')
            ->assertStatus(400)
            ->assertJson([
                'status'  => 0,
                'message' => null,
                'errors'  => [
                    'first_name' => [
                        'The first name field is required.'
                    ],
                    'last_name' => [
                        'The last name field is required.'
                    ],
                    'email' => [
                        'The email field is required.'
                    ],
                    'password' => [
                        'The password field is required.'
                    ],
                    'address' => [
                        'The address field is required.'
                    ],
                    'phone_number' => [
                        'The phone number field is required.'
                    ]
                ],
                'data' => null
            ]);
    }

    public function testsRequirePasswordConfirmation()
    {
        $faker = Faker::create();

        $password = $faker->password(6);

        $payload = [
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'avatar'       => null,
            'email'        => $faker->email,
            'password'     => $password,
            'address'      => $faker->address,
            'phone_number' => $faker->phoneNumber,
            'is_marketing' => false,
        ];

        $this->json('post', '/api/v1/admin/create', $payload)
            ->assertStatus(400)
            ->assertJson([
                'status'  => 0,
                'message' => null,
                'errors'  => [
                    'password' => [
                        "The password confirmation does not match."
                    ]
                ],
                'data' => null
            ]);
    }
}

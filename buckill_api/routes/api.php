<?php

use Illuminate\Support\Facades\Route;
use App\Apis\V1\Users\Http\Controllers\AuthController;
use App\Apis\V1\Users\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('unauthenticated', [AuthController::class, 'unauthenticated'])->name('unauthenticated');

Route::prefix('/v1')
    ->group(function () {

        Route::post('admin/login', [AuthController::class, 'login'])->name('login');
        Route::post('admin/create', [AuthController::class, 'register']);

        Route::group([
            'middleware' => 'auth:api'
        ], function () {
            Route::get('user',[UsersController::class, 'getUser']);

            Route::get('admin/logout',[AuthController::class, 'logout']);
        });

    });

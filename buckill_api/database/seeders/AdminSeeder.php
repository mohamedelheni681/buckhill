<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        User::create([
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'is_admin' => User::IS_ADMIN,
            'email' => config('petshop.admin_email'),
            'email_verified_at' => now(),
            'password' => Hash::make(config('petshop.admin_password')),
            'address' => $faker->address,
            'phone_number' => $faker->phoneNumber,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

}
